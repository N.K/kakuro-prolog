
line(L,N) :-
    alldigits(L),	%για να είναι οι αριθμοί απο [1-9]
    alldifferent(L), %για να είναι κάθε αριθμός διαφορετικός
    listsum(L,N).		%βρίσκει το άρθροισμα των στοιχείων της λίστας 


alldigits([]).
alldigits([X|Y]) :- 
    digit1(X),

    alldigits(Y).    

alldifferent([]).
alldifferent([X|Y]):-
    alldifferent(Y),
    notmember(X,Y).


listsum([],0).
listsum([X|Y],N):-
    listsum(Y,N1),
    N is N1 + X.



digit1(X) :- 
    member(X,[1,2,3,4,5,6,7,8,9]).


member(X,[X|_]).
member(X,[U|V]):-
    member(X,V).




notmember(X,[]).
notmember(X,[U|V]):-
    differ(X,U),
    notmember(X,V).




differ(X,Y) :- X \== Y.
    



kakuro_all(Ranges, Sums) :-
	maplist(kakuro_1D, Ranges, Sums).

	

kakuro_1D(List, Sum) :-
	  	alldigits(List),	
    		alldifferent(List), 
   		listsum(List,Sum).


solve(List) :-

	List = [
		[A, B, C],				%Η λίστα που αντιστοιχει τα γράμματα με το ανάλογο άρθροισμα
		[E, F, G],
			[J, K, L],
			[N, O, P],
		[A, E],
        [B, F, J, N],
        [C, G, K, O],
		[L, P]
	],
	Sums = [24, 11, 22, 14, 17, 26, 15, 13],
	kakuro_all(List, Sums).


